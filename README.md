# MuniPortal

Show the arrival times of your favorite buses on [PyPortal](https://www.adafruit.com/product/4116) using the public JSON data from NextBus.com.

Included minified libraries from [adafruit-circuitpython-bundle-4.x-mpy-20191121.zip](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases/download/20191121/adafruit-circuitpython-bundle-4.x-mpy-20191121.zip)

Tested with CircuitPython 4.1 and ESP firmware 1.5.0.

Installation:

- Copy files to your mounted CIRCUITPY device.

Setup:

- Add your WiFi SSID and password to secrets.py
- Customize which buses you want to monitor by changing STOPS in code.py
