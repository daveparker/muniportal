# The MIT License (MIT)
#
# Copyright (c) 2019 Dave Parker
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
PyPortal MUNI bus monitor.

Show the arrival times of your favorite buses using the public JSON data from
NextBus.com.

Alter AGENCY and STOPS to customize.

"""

import json
import time
import board
import displayio

from adafruit_pyportal import PyPortal
from adafruit_display_text.label import Label
from adafruit_bitmap_font import bitmap_font
from analogio import AnalogIn

BACKGROUND = "muni.bmp"
PREDICTIONS_TO_DISPLAY = 3
FETCH_INTERVAL = 60
DISPLAY_INTERVAL = 5

SMALL_FONT_PATH = "/fonts/Arial-16.bdf"
MEDIUM_FONT_PATH = "/fonts/Arial-Bold-24.bdf"
LARGE_FONT_PATH = "/fonts/Roboto-Bold-50.bdf"

BASE_URL = ("http://webservices.nextbus.com/service/publicJSONFeed" +
            "?command=predictionsForMultiStops&a={agency}")

AGENCY = "sf-muni"

# List of "service|stop_id" to monitor.
# Service and stop IDs can be found on NextBus.com
# URL for service=24 and stop_id=3514 (first one listed in STOPS)
# https://www.nextbus.com/#!/sf-muni/24/24___I_F00/3141/3514
STOPS = ["24|3514", "35|4378", "35|4379", "52|4368"]


class Task():
    '''Run tasks periodically. Must be subclassed.'''
    def __init__(self, interval):
        self.interval = interval
        self.last_time = None

    def update(self):
        '''Run task if at last the interval time has elapsed'''
        now = time.monotonic()
        if self.last_time is None or now - self.last_time > self.interval:
            self.last_time = now
            self._run()

    def _run(self):
        pass # override in subclass


class DataTask(Task):
    '''Task to fetch and update data from NextBus'''
    def __init__(self, pyportal, stack, interval):
        super().__init__(interval)
        self.pyportal = pyportal
        self.stack = stack

    def _run(self):
        data = self.pyportal.fetch()
        data = json.loads(data)
        stops = self._parse_json(data)
        for i, _ in enumerate(stops):
            self.stack[i].update(stops[i])

    @staticmethod
    def _parse_json(data):
        stops = []
        for prediction in data["predictions"]:
            stop = Stop(*[prediction[x] for x in [
                "stopTitle", "routeTitle", "routeTag"]])
            stop.direction = prediction.get("dirTitleBecauseNoPredictions", None)

            directions = prediction.get("direction", [])
            if isinstance(directions, dict):
                directions = [directions]

            for direction in directions:
                direction_name = direction["title"]
                arrivals = direction["prediction"]
                if isinstance(arrivals, dict):
                    arrivals = [arrivals]

                for arrival in arrivals:
                    stop.predictions.append(
                        Prediction(int(arrival['minutes']), direction_name))

            stop.predictions.sort(key=lambda x: x.arrival)
            stops.append(stop)

        return stops


class DisplayTask(Task):
    '''Task to cycle through bus stop predictions'''
    def __init__(self, pyportal, stack, interval):
        super().__init__(interval)
        self.pyportal = pyportal
        self.stack = stack
        self.i = 0

    def _run(self):
        self.pyportal.splash[-1] = self.stack[self.i%len(self.stack)].group
        self.i += 1


class BacklightTask(Task):
    '''Task for updating the backlight based on lightsensor value'''
    def __init__(self, interval):
        super().__init__(interval)
        board.DISPLAY.auto_brightness = False
        self.light_sensor = AnalogIn(board.LIGHT)

    def _run(self):
        light = self.light_sensor.value
        if light > 10000:
            board.DISPLAY.brightness = 1.0
        else:
            board.DISPLAY.brightness = max(0.1, light / 10000.0)


class Stop():
    '''Data for a bus stop and route'''
    def __init__(self, name, route, route_id):
        self.name = name
        self.route = route
        self.route_id = route_id
        self.predictions = []
        self.direction = None


class Prediction():
    '''Data for an arrival prediction'''
    def __init__(self, arrival, direction):
        self.arrival = arrival
        self.direction = direction


class Card():
    '''A group of labels to show stop data'''
    def __init__(self, large_font, medium_font, small_font):

        self.group = displayio.Group(max_size=4)

        self.route_label = Label(medium_font, max_glyphs=20)
        self.route_label.x = 5
        self.route_label.y = 15
        self.route_label.color = 0x007700
        self.group.append(self.route_label)

        self.direction_label = Label(small_font, max_glyphs=30)
        self.direction_label.x = 5
        self.direction_label.y = 42
        self.group.append(self.direction_label)

        self.predictions_label = Label(large_font, max_glyphs=10)
        self.predictions_label.y = 120
        self.group.append(self.predictions_label)

        self.stop_label = Label(small_font, max_glyphs=30)
        self.stop_label.x = 5
        self.stop_label.y = 210
        self.group.append(self.stop_label)

    def update(self, stop):
        '''Set text labels for provided stop data'''
        update_text(self.route_label, stop.route)
        # Note: Show the direction of the next arriving bus.
        # Later busses may have different end points.
        if stop.predictions:
            update_text(self.direction_label, stop.predictions[0].direction)
        elif stop.direction is not None:
            update_text(self.direction_label, stop.direction)


        if PREDICTIONS_TO_DISPLAY < len(stop.predictions):
            predictions = stop.predictions[:PREDICTIONS_TO_DISPLAY]
        else:
            predictions = stop.predictions

        if stop.predictions:
            update_text(self.predictions_label, ", ".join([str(x.arrival) for x in predictions]))
        else:
            update_text(self.predictions_label, "None")

        center(self.predictions_label)
        update_text(self.stop_label, stop.name)


def center(label):
    '''Centers a label horizontally on the display'''
    _, _, width, _ = label.bounding_box
    label.x = int((320 - width) / 2)


def update_text(label, text):
    '''Update label text, truncating it to the label's capacity if necessary'''
    label.text = text[:label.width] if len(text) > label.width else text


def main():
    '''Do all the things'''
    url = BASE_URL.format(agency=AGENCY)
    url += ''.join(['&stops={}'.format(stop) for stop in STOPS])

    pyportal = PyPortal(url=url,
                        json_path=[],
                        status_neopixel=board.NEOPIXEL,
                        default_bg=BACKGROUND)

    glyphs = b'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-+,. "\''

    small_font = bitmap_font.load_font(SMALL_FONT_PATH)
    small_font.load_glyphs(glyphs)

    welcome_label = Label(small_font, text="Loading...")
    welcome_label.y = 120
    center(welcome_label)
    pyportal.splash.append(welcome_label)
    welcome_label = None

    medium_font = bitmap_font.load_font(MEDIUM_FONT_PATH)
    medium_font.load_glyphs(glyphs)

    glyphs = None

    large_font = bitmap_font.load_font(LARGE_FONT_PATH)
    large_font.load_glyphs(b'0123456789, ')

    # Each stop gets its own group of text labels to make flipping between them faster
    stack = []
    for _ in range(len(STOPS)):
        stack.append(Card(large_font, medium_font, small_font))

    data_task = DataTask(pyportal, stack, FETCH_INTERVAL)
    display_task = DisplayTask(pyportal, stack, DISPLAY_INTERVAL)
    backlight_task = BacklightTask(0)

    tasks = [data_task, display_task, backlight_task]

    while True:
        try:
            for task in tasks:
                task.update()
                time.sleep(1)

        except:
            print("Something untoward occurred!")
            time.sleep(FETCH_INTERVAL)


main()
